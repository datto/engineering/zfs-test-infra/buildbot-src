# This file is part of Buildbot.  Buildbot is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright Buildbot Team Members

import logging
import re
import warnings

from dateutil.parser import parse as dateparse
from twisted.python import log

try:
    import json
    assert json
except ImportError:
    import simplejson as json


_HEADER_EVENT = 'X-Gitlab-Event'
_HEADER_GITLAB_TOKEN = 'X-Gitlab-Token'


class GitLabEventHandler(object):
    def __init__(self, secret, codebase=None):
        self._secret = secret
        self._codebase = codebase

    def process(self, request):
        payload = self._get_payload(request)

        log.msg("X-Gitlab-Event: %r" % (request.getHeader(_HEADER_EVENT),), logLevel=logging.DEBUG)

        event_type = payload['object_kind']
        log.msg("identified as event_type: %r" % (event_type,), logLevel=logging.DEBUG)

        handler = getattr(self, 'handle_%s' % event_type, None)

        if handler is None:
            raise ValueError('Unknown event: %r' % (event_type,))

        return handler(payload)

    def _get_payload(self, request):
        content = request.content.read()
        event_type = request.getHeader(_HEADER_EVENT)

        if self._secret:
            received_secret = request.getHeader(_HEADER_GITLAB_TOKEN)
            if received_secret != self._secret:
                raise ValueError("Invalid secret")

        try:
            payload = json.loads(content)
        except Exception, e:
            raise ValueError("Error loading JSON: " + str(e))

        log.msg("Payload: %r" % payload, logLevel=logging.DEBUG)

        return payload

    def handle_push(self, payload):
        user = payload['user_name']
        repo = payload['repository']['name']
        repo_url = payload['repository']['url']
        project = payload['project']['path_with_namespace']

        changes = self._process_change(payload, user, repo, repo_url, project)

        log.msg("Received %d changes from GitLab" % len(changes))

        return changes, 'git'

    def handle_merge_request(self, payload):
        changes = []
        attrs = payload['object_attributes']
        number = attrs['iid']
        refname = 'refs/merge-requests/%d/head' % (number,)
        commit = attrs['last_commit']

        log.msg('Processing GitLab MR #%d' % number, logLevel=logging.DEBUG)

        action = attrs['action']
        if action not in ('open', 'reopen', 'update'):
            log.msg("GitLab MR #%d %s, ignoring" % (number, action))
            return changes, 'git'

        change = {
            'revision': commit['id'],
            'when_timestamp': dateparse(commit['timestamp']),
            'branch': attrs['source_branch'],
            'revlink': attrs['url'],
            'category': 'merge_request',
            'author': '%s <%s>' % (commit['author']['name'],
                                   commit['author']['email']),
            'comments': 'GitLab Merge Request#%d: %s\n\n%s' % (
                attrs['iid'], attrs['title'], attrs['description']),
            'repository': payload['repository']['url'],
            'project': payload['project']['path_with_namespace'],
            'properties': {
                'target_branch': attrs['target_branch'],
                'target_repository': attrs['target']['git_http_url'],
                'event': 'merge_request',
            },
        }

        if callable(self._codebase):
            change['codebase'] = self._codebase(payload)
        elif self._codebase is not None:
            change['codebase'] = self._codebase

        changes.append(change)

        log.msg("Received %d changes from GitLab MR #%d" % (
            len(changes), number))
        return changes, 'git'

    def _process_change(self, payload, user, repo, repo_url, project):
        """
        Consumes the JSON as a python object and actually starts the build.

        :arguments:
            payload
                Python Object that represents the JSON sent by GitLab Service
                Hook.
        """
        changes = []
        refname = payload['ref']

        # We only care about regular heads, i.e. branches
        match = re.match(r"^refs\/heads\/(.+)$", refname)
        if not match:
            log.msg("Ignoring refname `%s': Not a branch" % refname)
            return changes

        branch = match.group(1)
        if payload.get('deleted'):
            log.msg("Branch `%s' deleted, ignoring" % branch)
            return changes

        for commit in payload['commits']:
            if not commit.get('distinct', True):
                log.msg('Commit `%s` is a non-distinct commit, ignoring...' %
                        (commit['id'],))
                continue

            files = []
            for kind in ('added', 'modified', 'removed'):
                files.extend(commit.get(kind, []))

            when_timestamp = dateparse(commit['timestamp'])

            log.msg("New revision: %s" % commit['id'][:8])

            change = {
                'author': '%s <%s>' % (commit['author']['name'],
                                       commit['author']['email']),
                'files': files,
                'comments': commit['message'],
                'revision': commit['id'],
                'when_timestamp': when_timestamp,
                'branch': branch,
                'revlink': commit['url'],
                'repository': repo_url,
                'project': project,
                'category': payload['object_kind'],
                'properties': {
                    'event': payload['object_kind'],
                },
            }

            if callable(self._codebase):
                change['codebase'] = self._codebase(payload)
            elif self._codebase is not None:
                change['codebase'] = self._codebase

            changes.append(change)

        return changes


def getChanges(request, options=None):
    """
    Reponds only to POST events and starts the build process

    :arguments:
        request
            the http request object
    """
    if options is None or (isinstance(options, bool) and options):
        if options is not None:
            warnings.warn('Use of True for GitLab hook is deprecated.  '
                          'Please see the documentation for possible values')
        options = {}

    klass = options.get('class', GitLabEventHandler)

    handler = klass(options.get('secret', None),
                    options.get('codebase', None))
    return handler.process(request)
